﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Validation.Models;

namespace Validation.Validators
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            ValidatorOptions.CascadeMode =
                FluentValidation.CascadeMode.StopOnFirstFailure;

            When(user => user.Password.Length >= 8, () =>
            {
                RuleFor(u => u.Password).Custom((field, context) =>
                {
                    if (!char.IsUpper(field[0]))
                    {
                        context.AddFailure("Password length > 8. Password must start with a capital letter");
                    }
                });
            });
            
            // Or
            
            //When(user => user.Password.Length >= 8, () =>
            //{
            //    RuleFor(u => u.Password).FirstLetterIsCapital();
            //});
        }

    }
}
