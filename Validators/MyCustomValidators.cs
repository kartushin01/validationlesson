﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace Validation.Validators
{
    public static class MyCustomValidators
    {
        public static IRuleBuilderOptions<T, IList<TElement>> ListMustContainFewerThan<T, TElement>(this IRuleBuilder<T, IList<TElement>> ruleBuilder, int num)
        {
            return ruleBuilder.Must(list => list.Count < num).WithMessage("The list contains too many items");
        }
        public static IRuleBuilderOptions<T, TElement> FirstLetterIsCapital<T, TElement>(this  IRuleBuilder<T, TElement> ruleBuilder) 
        {
            return ruleBuilder.Must(field => char.IsUpper(Convert.ToChar(field.ToString().Substring(0, 1)))).WithMessage("Password must start with a capital letter"); ;
        }
    }
}
