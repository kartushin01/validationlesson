﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Validation.Models
{
    public class User
    {

        [Required]
        public string Name { get; set; }

        [Remote("VerifyLength", "Form", HttpMethod = "POST", ErrorMessage = "Username and password are the same length", AdditionalFields = nameof(Name) + "," + nameof(Password))]
        [Required]
        [MinLength(8)]
        public string Password { get; set; }
        [Remote("VerifyPhone", "Form")]
        public string Phone { get; set; }
    }
}
