﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Validation.Models;


namespace Validation.Controllers
{
    public class FormController : Controller
    {

        public IActionResult Index()
        {

            return View();
        }

      
        [HttpPost]
        public IActionResult GetForm(string name, string password)
        {
            ViewData["Name"] = name;
            ViewData["Password"] = password;

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(password))
            {
                ViewData["Error"] = "name or password is empty";
                return View("Index");
            }

            return View("Result");
        }

        public IActionResult IndexWhithModel()
        {
            return View("IndexWhitModel");
        }


        [HttpPost]
        public IActionResult GetFormWhithModel(User model)
        {

            if (!ModelState.IsValid)
            {
                return View("IndexWhitModel", model);
            }

            return View("ResultWhithModel", model);

        }


        [AcceptVerbs("GET", "POST")]
        public IActionResult VerifyLength(User model)
        {
            if (!string.IsNullOrEmpty(model.Name) && !string.IsNullOrEmpty(model.Password))
            {
                if (model.Name.Length == model.Password.Length)
                {
                    return Json(false);
                }
            }

            return Json(true);
        }


        [AcceptVerbs("GET", "POST")]
        public IActionResult VerifyPhone([RegularExpression(@"^\+79\d{9}$")] string phone)
        {

            if (!ModelState.IsValid)
            {
                return Json($"Phone {phone} has an invalid format. Format: +79#########");
            }

            return Json(true);
        }
    }
}
